using namespace System.Net
param($Request, $TriggerMetadata)
$body = [PSCustomObject]@{
    DeploymentModel = 'Blue-Green-Deployment'
    Contact         = 'Chen V'
    Status          = 'GreenBlue!'
} | ConvertTo-Json -Compress

Push-OutputBinding -Name Response -Value ([HttpResponseContext]@{
        StatusCode = [HttpStatusCode]::OK
        Body       = $body
    })